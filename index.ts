// 1. Add typings to the fruitBasket constant

enum Fruit {
    BANANA = 'banana',
    ORANGE = 'orange',
    KIWI = 'kiwi',
    APPLE = 'apple'
}

const fruitBasket: FruitBasket = {
    banana: 2,
    orange: 3,
    kiwi: 2,
    apple: 3
}

type FruitBasket = {
    [key in Fruit]: number
}

// 2. Add typings to the Person class

interface Introduce {
    introduce: (string: string[]) => string
}

class Person implements Introduce {
    name: string;
    gender: string;
    age: number;
    likes: string[];

    public constructor(name: string, gender: string, age: number, likes: string[]) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.likes = likes;
    }

    public introduce(): string {
        const {name, gender, age, likes} = this;
        const goodLookingMap = new Map<string, string>([['male', 'handsome'], ['female', 'cute']]);
        return `Hello, I'm ${name}, ${age} years old, I like: ${likes.join(', ')}.
        As you can see, I'm quite ${goodLookingMap.get(gender)} too!`;

    }
}

const Dima : Person = new Person('Dima', 'male', 22, ['video games', 'martial arts']);

// 3. Add typings to MovieService class

class MovieService<T> {
    logger: T;

    constructor(logger: T) {
        this.logger = logger;
    }

    public getMovies(): Promise<string[]> {
        return Promise.resolve(['Jaws', 'Spider-Man'])
            .catch(err => {
                this.logger.log(err);
                return [];
            })
    }
}

class LoggerOne {
    public log(err: Error) {
        console.log('sending logs to log storage 1', err);
    }
}

class LoggerTwo {
    public log(err: Error) {
        console.log('sending logs to log storage 2', err);
    }
}

const movieService1 = new MovieService<LoggerOne>(new LoggerOne());
const movieService2 = new MovieService<LoggerTwo>(new LoggerTwo());
